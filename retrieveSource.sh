#export SRCGIT=https://gitlab.com/SRombauts/SQLiteCpp.git
export SRCGIT="--branch fixes_by_KOLANICH --single-branch https://gitlab.com/KOLANICH/SQLiteCpp.git"
git clone --depth=1 $SRCGIT
shopt -s dotglob
cp -rlf ./SQLiteCpp/* ./
rm -rf ./SQLiteCpp
cp ./Hardening.cmake ./cmake/
